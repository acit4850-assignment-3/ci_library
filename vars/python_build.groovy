def call(dockerRepoName, imageName, portNum) {
    pipeline {
        agent any
        parameters {
            booleanParam(defaultValue: false, description: 'Deploy the App', name: 'DEPLOY')
        }
        stages {
            stage('Lint') {
                steps {
                    sh 'pylint-fail-under --fail_under 5 *.py'
                }
            }
            stage('Package') {
                when {
                    expression { env.GIT_BRANCH == 'origin/main' }
                }
                steps {
                    withCredentials([string(credentialsId: 'maryamtaer', variable: 'TOKEN')]) {
                        sh "docker login -u 'maryamtaer' -p '$TOKEN' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag maryamtaer/${dockerRepoName}:${imageName} ."
                        sh "docker push maryamtaer/${dockerRepoName}:${imageName}"
                    }
                }
            }
            stage('Deliver & Scan') {
                when {
                    expression { params.DEPLOY }
                }
                steps {
                    sh "docker stop ${dockerRepoName} || true && docker rm ${dockerRepoName} || true"
                    sh "docker run -d -p ${portNum}:${portNum} --name ${dockerRepoName} ${dockerRepoName}:latest || true"
                    sh "docker ps"
                    sh "docker inspect -f '{{json .State}}' \$(docker ps -aqf name=${dockerRepoName}) || true"
                    sh "docker inspect --format='{{json .NetworkSettings.Networks}}' \$(docker ps -aqf name=${dockerRepoName}) || true" 
                    sh "docker inspect ${dockerRepoName}:latest > containerInspectInfo.txt"                   
                }
            }
            stage('Zip Artifacts') {
                steps {
                    sh 'zip artifact.zip *.*'
                    archiveArtifacts artifacts: 'artifact.zip'
                }
           }
        }
    }
}
